import * as React from "react";
import { ChangeEvent } from "react";
import { ReactElement } from "react";
// use redux to store user data?
type Props = {
  username: string | null;
  setUsername: React.Dispatch<React.SetStateAction<string | null>>;
  password: string | null;
  setPassword: React.Dispatch<React.SetStateAction<string | null>>;
};
const Login = ({
  username,
  setUsername,
  password,
  setPassword,
}: Props): ReactElement => {
  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    console.log(username, password);
  };
  const handleUsernameChange = (event: ChangeEvent<HTMLInputElement>) => {
    const element = event.currentTarget as HTMLInputElement;
    setUsername(element.value);
    // this also work?
  };
  const handlePasswordChange = (event: ChangeEvent<HTMLInputElement>) => {
    setPassword(event.currentTarget.value);
    // this is a good way
  };
  return (
    <div>
      <form onSubmit={handleSubmit}>
        <label htmlFor="username">
          email
          <input
            onChange={handleUsernameChange}
            type="email"
            value={username === null ? "" : username}
          />
        </label>
        <label htmlFor="password">
          password
          <input
            onChange={handlePasswordChange}
            type="password"
            value={password === null ? "" : password}
          />
        </label>
        <button>Log in </button>
      </form>
    </div>
  );
};

export default Login;
