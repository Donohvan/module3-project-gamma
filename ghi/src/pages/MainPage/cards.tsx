const Cards = () => {
  return (
    <section className="flex flex-col md:flex-row">
      <div>
        <h1 className="text-">Believe the hype.</h1>
        <p>
          Spend more time providing memorable customer experiences. Your free
          online booking system handles the legwork so you don’t have to.
        </p>
      </div>
      <div>2</div>
    </section>
  );
};

export default Cards;
