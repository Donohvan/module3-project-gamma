import Heading from "./components/Heading";
// import * as React from "react";
import { useState } from "react";
import Login from "./components/Login";
import MainPage from "./pages/MainPage/MainPage";

function App() {
  const title = "HELLO WORLD";
  const [username, setUsername] = useState<string | null>(null);
  const [password, setPassword] = useState<string | null>(null);

  return (
    <div className="bg-sky-50 min-w-full">
      <Heading title={title} />
      <Login
        username={username}
        setUsername={setUsername}
        password={password}
        setPassword={setPassword}
      />
      <MainPage />
    </div>
  );
}
export default App;
